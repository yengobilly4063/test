import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var config = {
    apiKey: "AIzaSyDp9dIOQJUqGbg9MkOW313sl2WbYDZBOsw",
    authDomain: "udemy-bill-smoothies.firebaseapp.com",
    databaseURL: "https://udemy-bill-smoothies.firebaseio.com",
    projectId: "udemy-bill-smoothies",
    storageBucket: "udemy-bill-smoothies.appspot.com",
    messagingSenderId: "70188054622",
    appId: "1:70188054622:web:475f9e3d68ff5bac9c1ccf",
    measurementId: "G-6BBFFPQ30K"
  };
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(config);
  // firebaseApp.firestore().settings({ timestampsInSnapshots: true})

  //export firestore database
  export default firebaseApp.firestore()

